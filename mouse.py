#!/usr/bin/python

import struct, os, time

file = open( "/dev/input/mice", "rb" );

flag = False
lastTime = 0.0

def getMouseEvent():
  global flag
  buf = file.read(3);
  button = ord( buf[0] );
  bLeft = button & 0x1;
  bMiddle = ( button & 0x4 ) > 0;
  bRight = ( button & 0x2 ) > 0;
  x,y = struct.unpack( "bb", buf[1:] );
  #print ("L:%d, M: %d, R: %d, x: %d, y: %d\n" % (bLeft,bMiddle,bRight, x, y) );
  if (bLeft!=0 or bRight!=0) and flag != True:
    flag = True
    lastTime = time.time()
    os.system("omxplayer "+os.path.join("/home","pi","Deter.mp3")+" > /dev/null")

while( 1 ):
  time.sleep(0.5)
  if time.time() - lastTime > 10:
    getMouseEvent()
    flag = False

file.close();
