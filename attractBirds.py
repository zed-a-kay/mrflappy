#!/usr/bin/python

import urllib
import json, os
from random import randint

current_location = urllib.urlopen('http://freegeoip.net/json/')
current_location = json.load(current_location)
region = current_location['region_name']

birds = urllib.urlopen('http://www.xeno-canto.org/api/2/recordings?query=loc:{0}'.format(region))
birds = json.load(birds)
birds_list = birds['recordings']
birds_num =  len(birds_list)

while True:
	random_bird_idx = randint(0,birds_num-1)
	print random_bird_idx
	random_bird_recording = birds_list[random_bird_idx]['file']
#	with open("temp-bird-call.mp3","w") as f:
#		f.write(random_bird_recording)
	for x in range(0, randint(1,5)):
		os.system("omxplayer "+random_bird_recording)