SetOptions[$FrontEnd, 
NotebookBrowseDirectory->"/home/pi",
"DisplayImagePixels"->"Automatic",
Current2DTool->"Select",
Default2DTool->"Select",
Current3DTool->"RotateView",
Default3DTool->"RotateView",
EvaluatorNames->{"Local" -> {"AutoStartOnLaunch" -> True}},
NotebooksMenu->{"tst.nb" -> {
    FrontEnd`FileName[{$RootDirectory, "home", "pi"}, "tst.nb", 
     CharacterEncoding -> "UTF-8"], True, False, True}, "main.m" -> {
    FrontEnd`FileName[{$RootDirectory, "home", "pi"}, "main.m", 
     CharacterEncoding -> "UTF-8"], True, False, True}, "initialize.nb" -> {
    FrontEnd`FileName[{$RootDirectory, "home", "pi"}, "initialize.nb", 
     CharacterEncoding -> "UTF-8"], True, False, True}, "detectBird.nb" -> {
    FrontEnd`FileName[{$RootDirectory, "home", "pi"}, "detectBird.nb", 
     CharacterEncoding -> "UTF-8"], True, False, True}, "final.nb" -> {
    FrontEnd`FileName[{$RootDirectory, "home", "pi"}, "final.nb", 
     CharacterEncoding -> "UTF-8"], True, False, True}, "mouseEvent.nb" -> {
    FrontEnd`FileName[{$RootDirectory, "home", "pi"}, "mouseEvent.nb", 
     CharacterEncoding -> "UTF-8"], True, False, True}, "trainer.nb" -> {
    FrontEnd`FileName[{$RootDirectory, "home", "pi"}, "trainer.nb", 
     CharacterEncoding -> "UTF-8"], True, False, True}, "main.nb" -> {
    FrontEnd`FileName[{$RootDirectory, "home", "pi"}, "main.nb", 
     CharacterEncoding -> "UTF-8"], True, False, True}}
]
