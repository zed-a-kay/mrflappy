/**
* LAYOUT
*/
body {
	margin: 0;
	padding: 0;
  background: #344e74 url(../images/body2.png) 0 0 repeat-x;
	color: #000;
}
#pageWrap {
	font: 76%/1.5em Arial, Helvetica, sans-serif;
	width: 960px;
	margin: auto;
	background-color: #fff;
	border: 1px solid #ccc;
}
#innerColumn {
	width: 100%;
}
#soContainer {
	float: left;
	width: 95%;
}
#leftCol {
	float: left;
	margin-left: -15em;
	width: 200px;
	border-right: 1px solid #eeeeee;
	padding: 0 5px 0 0;
}
#rightCol {
	float: right;
	margin-right: -15em;
	width: 200px;
	border-left: 1px solid #eeeeee;
	padding: 0 0 0 5px;
}
#rightCol.down {
	margin-top: 300px;
}

#leftCol .inside {
	padding: 10px 0 10px 10px;
	position: relative;
}
#rightCol .inside {
	padding: 10px 10px 10px 0;
	position: relative;
}

.two-sidebars #outerColumn {
	border-left: 15em solid #fff;
	border-right: 15em solid #fff;
}
.sidebar-right #outerColumn {
	border-right: 15em solid #fff;
}
.sidebar-left #outerColumn {
	border-left: 15em solid #fff;
}


/* udel header and footer */
#UDStandardHeader #UDStandardHeader_LayoutContainer,
#UDStandardFooter #UDStandardFooter_LayoutContainer {
	padding: 0px;
	width: 960px;
}
#UDStandardFooter #UDStandardFooter_LayoutContainer #UDStandardFooter_Address li,
#UDStandardFooter #UDStandardFooter_LayoutContainer #UDStandardFooter_Links li,
#UDStandardFooter #UDStandardFooter_LayoutContainer #UDStandardFooter_Address li a,
#UDStandardFooter #UDStandardFooter_LayoutContainer #UDStandardFooter_Links li a {
	color: #fff;
}


#UDStandardFooter #UDStandardFooter_LayoutContainer #UDStandardFooter_Logo {
	background: url(../images/udlogo-footer.png) 0 0 no-repeat;
	height: 0 !important; 
	line-height: 1;
	overflow: hidden !important;
	padding-top: 59px;
	width: 136px;
	display: block;
}

/**
* GENERIC GLOBAL STYLES
*/

/* links */
a {text-decoration: none; font-weight: bold;}
a:link {/*color: #060C56;*/color: #3B71BF;}
a:visited {/*color: #157;*/ color: #3B71BF;}
a:hover {text-decoration: underline; color: rosyBrown;}


/* headings */
h1, h2, h3, h4, h5, h6 {
  margin: 2px 0 4px 0;
}
h1 {
  font-size: 1.6em;
  color: #060C56;
}
h2 {
  font-size: 1.25em;
  color: #060C56;
}
h3 {
  font-size: 1.1em;
  color: #060C56;
}
h4, h5, h6 {
  font-size: 1.1em;
}
p {
  margin-bottom: 0.9em;
  line-height: 1.5em;
}
label {
	cursor: pointer;
}
input, 
select, 
textarea {
	font: 100% Arial, Helvetica, sans-serif;
}
td {
	vertical-align: top;
}

.clr {
	clear: both;
}

div.taxonomy ul li.taxonomy_term_5, 
div.taxonomy ul li.taxonomy_term_6 {
	display: none;
}


/**
* FEATURED TOP
*/
#featured_top {
	margin-bottom: 3em;
}
#featured_top .block {
	background-color: #fff;
	border-bottom: none;
	border-top: none;
}
#content #featured_top .item-list {
	border-bottom: none;
	margin-bottom: auto;
}
#featured_top .view-display-id-block_2 .views-field-field-news-image-fid img {
	float: left;
	margin-right: 10px;
	margin-bottom: 10x;
}
#featured_top .views-field-field-altnewlink-value a {
	float: right;
	margin-right: 15px;
	font-size: 90%;
	margin-top: 5px;
}


/**
* FEATURED BOTTOM
*/
#featured_bottom {
	/*border: 1px solid #dddddd;
	padding: 10px 2px 10px 10px;*/
	clear: left;
	/*width: 48%;*/
}
#featured_bottom .block {
	background-color: #fff;
	border-bottom: none;
	border-top: none;
}
#featured_bottom .block h2 {
	font-family: Georgia, serif;
}
#content #featured_bottom .item-list {
	border-bottom: none;
	margin-bottom: auto;
}
#featured_bottom td.col-1 {
	padding-right: 10px;
	width: 50%;
}
#featured_bottom td.col-2 {
	padding-left: 10px;
	width: 50%;
}
#featured_bottom td img {
	float: left;
	margin-right: 5px;
	margin-top: 12px;
}
#featured_bottom .views-field-field-altnewlink-value a {
	float: right;
	margin-right: 15px;
	font-size: 90%;
	margin-top: 5px;
}
#featured_bottom table.views-view-grid tbody {
	border: none;
}


/**
* CONTENT
*/

#content {
	float: right;
	width: 95%;
	z-index: -9999;
}
#content .inside {
	padding: 10px;
}
#content .inside .node {
  margin: 0.5em 0 2em 0;
  overflow: hidden;
}
#content .inside .node .content {
  line-height: 1.5em;
  margin: 0.5em 0 0.5em 0;
}
#content .inside h1.title, #content .inside .view h2 {
	font-family: Georgia, serif;
}

#pageWrap .field-field-news-image img {
	float: left;
	padding-right: 15px;
}

/***
*  FIX FOR STATIC HTML SITE
*  Tina Callahan
*/
/* computing sites pages */
#soContainer.computingsites #content {
	float: right;
	width: 700px;
	z-index: -9999;
}
#soContainer.computingsites #leftCol {
	float: left;
	margin-left: 0;
	width: 200px;
	border-right: 1px solid #eeeeee;
	padding: 0 5px 0 0;
}
/**
* SIDEBARS
*/

/* links */
#leftCol a, 
#rightCol a { 
	font-weight: normal; 
	font-style: normal; 
}

/* views */
#leftCol .views-row,
#rightCol .views-row {
	margin-bottom: 5px;
}

/* menus */
#leftCol ul, 
#rightCol ul {
	margin: 0;
	padding: 8px 4px 8px 0;
}
#leftCol ul li, 
#rightCol ul li {
	margin: 0 0 0.25em 0.5em;
	list-style-image: none;
	list-style-type: none;
}
#leftCol ul li strong, 
#rightCol ul li strong {
	color: #060C56;
}
#leftCol ul ul, 
#rightCol ul ul {
	margin: 0;
	padding: 0;
}
#leftCol ul li li, 
#rightCol ul li li {
	margin: 0 0 0 0.9em;
	padding: 0 4px 0 0;
}


/* Blocks */
#leftCol .block, 
#rightCol .block {
	width: auto; 
	background: #ECF4FF url("../images/bgd_boxes7.png") repeat-x 0 0;
  border-top: 1px solid #cdd;
  padding-bottom: 0.5em;
  margin-bottom: 1.5em;
}
#leftCol .block .content, 
#rightCol .block .content {
	padding: 0 5px;
}

#block-block-4 {background: #ECF4FF url("../images/bgd_boxes_security8.png") repeat-x 0 0;}
#block-block-7 {background: #ECF4FF url("../images/bgd_boxes7-lg.png") repeat-x 0 0;}

/* Block titles */
#leftCol .block h2.title,
#rightCol .block h2.title {
	margin: 0;
	width:auto; 
	/*height: 19px; --removed 5/26/2010 */
	font-size:1em; 
	padding: 0.4em 0 0 0.6em;
	display:block;
	color: #FFF;
	text-transform: uppercase;
}
/* no icons needed 
#pageWrap #block-block-1 h2.title {background: url("../images/icon_services7.png") no-repeat 0 0; padding-left: 2.3em;}
#pageWrap #block-block-2 h2.title {background: url("../images/icon_resources7.png") no-repeat 0 0; padding-left: 2.3em;}
#pageWrap #block-block-3 h2.title {background: url("../images/icon_status7.png") no-repeat 0 0; padding-left: 2.3em;}*/
#pageWrap #block-block-4 h2.title {background: url("../images/Alert-16px.png") no-repeat 5px 6px; padding-left: 2.3em;}
/*#pageWrap #block-block-6 h2.title {display:block; background: url("../images/icon_services7.png") no-repeat 0 0; padding-left: 2.3em;}
#pageWrap #block-block-7 h2.title {background: url("../images/icon_status9.png") no-repeat 1px 4px; padding-left: 2.3em;}
#pageWrap #block-views-news-block_1 h2.title {background: url("../images/icon_news7.png") no-repeat 0 0; padding-left: 2.3em;}
#pageWrap #block-menu-menu-quicklinks h2.title {background: url("../images/icon_resources7.png") no-repeat 0 0; padding-left: 2.3em;}
#pageWrap #block-menu-menu-quicklinks h2.title {background: url("../images/icon_security8.png") no-repeat 0 0; padding-left: 2.3em;} */


/**
* MISC
*/

/* comments */
.comment {
  border: 1px solid #abc;
  padding: 0.5em;
  margin-bottom: 1em;
	font-size: 0.9em;
}
.comment .submitted {
	font-size: 0.9em;
}
.comment .content {
  margin: 0.5em 0 0.5em 0;
  line-height: 1.5em;
}
.comment .title a {
  font-size: 1.1em;
  font-weight: normal;
}
.comment .new {
  text-align: right;
  font-weight: bold;
  font-size: 0.8em;
  float: right;
  color: #f00;
}
.comment .picture {
  float: right;
}
.comment .links ul.links, 
.comment .links ul.links li {
	display: inline;
}


/* mission */
.mission {
  background: #cdd;
  padding: 1.5em 2em;
  color: #222;
	margin-bottom: 1em;
}
.mission a:link, 
.mission a:visited {
  color: #9cf;
}


/* help */
.help {
  font-size: 0.9em;
  margin-bottom: 1em;
	font-weight: bold;
}

/* breadcrumb */
#breadcrumb {
	background: #ececec;
	border-bottom: 1px solid #CCCCCC;
	padding: 2px 5px;
	color: #666;
	/*added*/
	font-family: Georgia, serif;
        display:none;
}
#breadcrumb a {font-weight: normal;}

/* tabs */
.tabs {
	margin-bottom: 1em;
}

/* messages */
.messages {
  background-color: #fc6;
  border: 1px solid #ccc;
  padding: 0.3em;
  margin-bottom: 1em;
}
.error {
  border-color: #f00;
}

/* notes (found on help topics pages */
div.note {
  border: 1px solid rgb(201, 207, 201);
  margin: 10px 0 10px 0;
  padding: 5px;
  background-color: rgb(246, 246, 204); }

div.note h1 {
  margin: 0;
  padding: 0;
  color: rgb(175, 30, 45);
  font-size: 1em;
  font-weight: bold; }

div.note p {
  margin: 0;
  padding: 0; }

/* tables */
table {
  font-size: 1em;
}
tr.odd td,
tr.even td {
  padding: 0.3em;
}


/* forms */
fieldset {
  border: 1px solid #ccc;
}
pre {
  background-color: #eee;
  padding: 0.75em 1.5em;
  font-size: 12px;
  border: 1px solid #ddd;
}

.form-item label {
  font-size: 1em;
  color: #222;
}
#content .item-list {
	font-size: 0.9em;
	margin: 1em 0;
	border-bottom: 1px solid #ccc;
}
.item-list .title {
  font-size: 1em;
  color: #222;
}

.links {
  clear: both;
	color: #999;
	font-size: 0.9em;
}
.sticky {
  padding: 0.5em;
  background-color: #f9f9f9;
  border: solid 1px #ddd;
	margin-top: 0;
}

.node .taxonomy {
  color: #999;
  font-size: 0.85em;
  text-align: right;
  display: none;
}
.submitted {
	font-size: 0.9em;
	font-style: italic;
}

.signature {
  padding: 0.5em;
	font-size: 0.9em;
	background: #cdd;
	margin: 1em;
}


.node .picture {
  float: right;
}

#profile .profile {
  clear: both;
  border: 1px solid #abc;
  padding: 0.5em;
  margin: 1em 0em 1em 0em;
}
.profile h3 {
	border: none;
	margin-bottom: 1em;
}
#profile .profile .name {
  padding-bottom: 0.5em;
}
#profile .profile .field {
  font-size: 0.9em;
	font-style: italic;
}
#book-outline {
	min-width: 1em;
	width: auto;
}
div.admin .left, 
div.admin .right {
	margin: 0;
	width: 49%;
}
div.admin-panel {
	padding: 10px 0;
}
div.admin-panel .description {
  color: #999;
}
div.admin-panel .body {
  background: #f4f4f4;
}
div.admin-panel h3 {
  background-color: #69c;
  color: #fff;
  padding: 5px 8px 5px;
  margin: 0;
}
.poll .vote-form {
	text-align: left;
}
.poll input {
	position: relative;
	top: 2px;
}
.poll .vote-form .choices {
	display: block;
}
.block-poll div.title {
	font-size: 1.1em;
  color: #d72;
}
#user-login-form {
	text-align: left;
}
.block #user-login-form ul {
	padding: 0;
}
#leftCol .item-list ul li.openid-link, 
#rightCol .item-list ul li.openid-link, 
#leftCol #user-login-form .item-list ul li, 
#rightCol #user-login-form .item-list ul li {
	margin-left: 0;
	list-style-type: none;
}
#user-login-form li.openid-link, 
#user-login li.openid-link {
	padding-left: 1.5em;
	background-position: left center;
	margin-left: 0;
}

/* QUICK TABS override background style */
.block.block-quicktabs, 
.block.block-quicktabs .block.block-block,
.block.block-quicktabs .block.block-gcal_events,
.block.block-quicktabs .item-list {
	background-color: #ffffff;
	border: none!important;
}
/* BLOCKTHEME settings */
.whiteblock { background-color: #fff!important; }

.simpleblock { background: none!important; margin-top: 10px;}
#leftCol .block.simpleblock, 
#rightCol .block.simpleblock { background: none; border-top: none!important;}
.simpleblock h2.title { text-transform: uppercase; font-family: serif; font-size: 1.1em!important; border-bottom: 1px solid #ddd!important; color: #666!important;}

.bluebar h2.title {
	margin: 0; 
	width:auto; 
	height: auto; 
	font-size:1.1em; 
	padding:0.4em 0 0 0.6em;	
	display:block; 
	color: #FFF; 
	text-transform: uppercase;
}


.view-display-id-page_1 td {
	padding: 10px;
}
.view-display-id-page_1 h2 {
 color: #2961E7;
 border-bottom: 2px solid #999999;
 line-height: 38px;
 padding-left: 45px;
}
.view-display-id-page_1 h2.ATS {background: transparent url('../images/icons/ATS_icon.gif') no-repeat;}
.view-display-id-page_1 h2.CSS {background: transparent url('../images/icons/CSS_icon.gif') no-repeat;}
.view-display-id-page_1 h2.CTO {background: transparent url('../images/icons/CTO_icon.gif') no-repeat;}
.view-display-id-page_1 h2.ISO {background: transparent url('../images/icons/ISO_icon.gif') no-repeat;}
.view-display-id-page_1 h2.ITCG {background: transparent url('../images/icons/ITCG_icon.gif') no-repeat;}
.view-display-id-page_1 h2.ITHC {background: transparent url('../images/icons/ITHC_icon.gif') no-repeat;}
.view-display-id-page_1 h2.ITIG {background: transparent url('../images/icons/ITIG_icon.gif') no-repeat;}
.view-display-id-page_1 h2.ITWD {background: transparent url('../images/icons/ITWD_icon.gif') no-repeat;}
.view-display-id-page_1 h2.MIS {background: transparent url('../images/icons/MIS_icon.gif') no-repeat;}
.view-display-id-page_1 h2.NSS {background: transparent url('../images/icons/NSS_icon.gif') no-repeat;}
.view-display-id-page_1 h2.UMS {background: transparent url('../images/icons/UMS_icon.gif') no-repeat;}
.view-display-id-page_1 h2.CIO {background: transparent url('../images/icons/CIO_icon.gif') no-repeat;}

table.views-view-grid td.col-1 { width: 48%; padding: 10px 2px 10px 10px; border: 1px solid #ddd; }
table.views-view-grid td.col-2 { width: 48%; padding: 10px 2px 10px 10px; border: 1px solid #ddd; }

.node .content span.featureImage img {
	float: left;
	margin-left: -5px;
}
.node .content span.pubDate {
	float: left;
}
#content .view-id-news table.views-view-grid .views-field-field-news-image-fid img {
	margin: 5px 5px 5px 0;
}
#content .view-id-news table.views-view-grid .views-field-tid {
	background:#ECF4FF none repeat scroll 0 0;
	border:1px dashed #CCCCCC;
	margin:0 auto;
	padding:2px 7px;
	/*width:98%;*/
	clear: left;
}
#content .view-id-news .view-display-id-page_1 table.views-view-grid td.col-1{
	border-bottom:1px solid #CCCCCC;
	padding:20px 0 2px;
}
#content .view-id-news table.views-view-grid td.col-1 p, 
#content .view-id-news table.views-view-grid td.col-2 p {
	margin-top: 0;
}
#content .view-id-news  table.views-view-grid td.col-1 h2 {
	border-bottom: 0px solid #999999;
	padding: 0;
	line-height: 1.3em;
}

#content div.image-caption {
	background-color:#FFFFFF;
	border:1px solid #CCCCCC;
	font-size:9pt;
	margin:5px;
	padding:7px;
}
#content div.image-caption.right {
  float: right;
}
#content div.image-caption.left {
  float: left;
}
.float-image-left {
  float: left;
  margin: 5px 5px 5px 0;
  /*padding: 5px;*/
  border: 1px solid #dddddd;
}
span.float-image-left img {
  margin:  5px!important;
}
/**
* MAIN NAVIGATION (where is this?)
*/
#main-navigation {
	background-color:#EFEFEF;
	border-bottom:1px solid #BAC2C6;
}
#main-navigation-inner {
	position:relative;
}
#main-navigation ul {
	list-style-image:none;
	list-style-position:outside;
	list-style-type:none;
	margin:0;
	padding:0 0 0 20px;
}
#main-navigation ul li {
  list-style: none;
  padding-top: 2px;
}
#main-navigation ul li a:link,
#main-navigation ul li a:visited {
	border-left:1px solid #BAC2C6;
	color:#000066;
	font-size: 1em;
}
#main-navigation ul li:hover a {
	background:#FFFFFF url(../images/nav-arrow-on.gif) no-repeat scroll 50% bottom;
	color:#000000;
}
#main-navigation ul li a {
	border-left:1px solid #CCCCCC;
	display:block;
	padding:7px 15px 7px 16px;
	text-decoration:none;
}
#main-navigation ul li.last {
	border-right: 0 none;
}
#main-navigation ul li.last a {
	border-right:1px solid #BAC2C6;
}


/* Top Navigation */
/*#topnav {width: 100%;border:none;}*/
#topnav {border:none; height: 1.2em;background: #000000 url('../images/nav-background-gradient3.png') repeat-x 0 0; color: #ffffff; width: 960px; margin: 0 auto; padding: 2px 0;}

#topnav ul { float: right; margin-top: 3px; margin-right: 10px;}

#topnav a {text-transform: uppercase; font-size: 12px; font-family: Georgia, serif;}
#topnav a:link, #topnav a:visited {color: #ffffff; text-decoration: none;}
#topnav a:hover { color: #ffffff; text-decoration: underline; }

/* first level */
ul.dropdown {position: relative; margin: 0; padding: 0; width: 100%; /*height: 22px*/; background-color: #EFEFEF; z-index: 10;}
ul.dropdown li {font-weight: bold; float: left; width: 85px; /*height: 22px*/; list-style: none; background-color: #EFEFEF;}
ul.dropdown li a {display: block; line-height: 1.7em;height: 1.7em; text-align: center; border-right: 1px solid #BAC2C6; color: #333; }
ul.dropdown li a:hover	{color: #FFF; text-decoration: none;}
ul.dropdown li:last-child a {border-right: none; } /* Doesn't work in IE */
ul.dropdown li.hover, 
ul.dropdown li:hover {background-color: #BAC2C6; color: #FFF; position: relative; }

/* second level */
ul.dropdown ul {width: 220px; visibility: hidden; position: absolute; top: 100%; left: 0; }
ul.dropdown ul li {font-weight: normal; font-size: 88%; background: #EFEFEF; color: #333; width: auto; border-bottom: 1px solid #DDD; float: none; }
 /* IE 6 and 7 Needs Inline Block */
ul.dropdown ul li a	{border-right: none; width: 100%; display: inline-block; color: #333; text-align: left; padding-left: 4px;} 
ul.dropdown ul li.hover a  {color: #FFF; text-decoration: none;}

