Features
==========

* Determine if there is a bird in the birdhouse (Wolfram image processing)

* Determine what type of bird is in the birdhouse (Wolfram Classifier)

* Tweet the image and type of bird (Wolfram twitter API)

Usage
======

* Run the `main.nb` and `mouse.py` files
